import java.util.Scanner;
public class Fibonacci {

	public static void main(String[] args) {
		Scanner fib=new Scanner(System.in);
		   System.out.println("Enter the Number");
		   int n=fib.nextInt();
		   int a, b, sum;
		   sum=0;
		   a=0;
		   b=1;
		   while(sum<n) {
			   System.out.println(sum +" ");
			   a=b;
			   b=sum;
			   sum = a+b;
         }
	}
}
