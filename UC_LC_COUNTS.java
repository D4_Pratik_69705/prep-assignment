import java.util.Scanner;

public class UC_LC_COUNTS {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String str = sc.next();
		int upper =0, lower=0, number=0, special=0;
		
		for(int i=0; i < str.length(); i++)
		{
		  char ch = str.charAt(i);
		  if(ch >= 'A' && ch<= 'Z')
			upper++;
		  else if(ch >= 'a' && ch <= 'z')
			lower++;
		  else if(ch >= '0' && ch<= '9')
			number++;
		  else
			special++;
		}
		System.out.println("Lower Case letters : " +lower);
		System.out.println("Upper case letters : " +upper);
		System.out.println("Numbers :" +number);
		System.out.println("Special characters :" +special);
				
	}
	

}
